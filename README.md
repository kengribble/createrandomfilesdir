##createrandomfilesdir
Create Random Files Directory

Creates a specific number of files, with random words of content, of random sizes, in a specified directory.


---

## Use

createrandomfilesdir.pl  [-dh] [-m max_size_number] <test folder> <number of files>

-d is debug mode

-h is help

-m max_size_number is the maximum file size (default is 10000)

---
