#!/usr/bin/perl
#
# adapted from code by: wazoox https://www.perlmonks.org/?node_id=292373
# may wazoox sled forever!
#
use strict;
use warnings;
use Carp;

my $min = 100;
use Getopt::Std;
our ($debug, $max);
our ($opt_d, $opt_h, $opt_m);
getopts('dhm:');
if ($opt_d){
        $debug = 1 ;
}
if ($opt_m) {
        $max = $opt_m;
}
if ($opt_h) {
    print "usage:\nmyprog [-h] [-d] [-i <seconds>]\n";
    print "usage : $0  [-dh] [-m max_size_number] <test folder> <number of files>\n-d is debug mode\n-h is help\n-m max_size_number is the maximum file size\n";
        exit;
}
$max = 10000 if not defined $max;
print "Debug mode on: $debug\n" if ($debug);
print "Max is: $max\n" if ($debug);
print "Minimum is hard set to: $min\n" if ($debug);


sub loaddict {
    my $dict = shift;

    open my $fh, $dict or croak "can't open $dict: $!";
    my @words = <$fh>;
    chomp @words;

    return \@words;
}

my $testdir = $ARGV[0]
    or croak "usage : $0 [-dh] [-m max_size_number] <test folder> <number of files>\n-d is debug mode\n-h is help\n-m max_size_number is the maximum file size\n";
my $filecount = $ARGV[1]
    or croak "usage : $0  [-dh] [-m max_size_number] <test folder> <number of files>\n-d is debug mode\n-h is help\n-m max_size_number is the maximum file size\n";
my $seed = 0;
$seed = $ARGV[2] if defined  $ARGV[2];

print "Directory is set to: $testdir\n" if ($debug);
print "Number of files is set to: $filecount\n" if ($debug);
# force number
$filecount += 0;

if ( not -d "$testdir" ) {
    mkdir "$testdir" or croak "can't mkdir $testdir";
}

my $wordlist = loaddict("/usr/share/dict/words");
#srand(42 + $seed );

for ( 1 .. $filecount ) {
    my $filename = $_ . ".txt";
    open my $fh, '>', "$testdir/$filename" or croak "can't open file $filename: $!";
    my $filesize = int( rand($max) ) + $min ;
    for ( 1 .. $filesize ) {
        my $dice = int( rand($#{$wordlist}) ) ;
        print $fh $wordlist->[$dice] . " ";
        if ( $_ % 12 == 0 ) {
            print $fh "\n";
        }
    }
}
